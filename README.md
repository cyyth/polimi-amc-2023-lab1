# Laboratory 1 – Magnetic Levitation System
One of the laboratories in «Advanced and Multivariable Control» (AMC) A.Y. 2022/2023

### The phase plane of the system
![alt text](figures/mag_lev_q2.jpg)

### The phase plane of the closed-loop system stabilized with Lyapunov-based feedback controller
![alt text](figures/mag_lev_q3.jpg)

### The phase plane of the closed-loop system stabilized with PI controller
![alt text](figures/mag_lev_q4.jpg)

# Included Contents
## Live Scripts:
- [main_mag_lev.mlx](main.mlx)

## Simulink Models:
- [mag_lev.slx](mag_lev.slx)
![alt text](figures/mag_lev.jpeg)

- [mag_lev_p_ctl.slx](mag_lev_p_ctl.slx)
![alt text](figures/mag_lev_p_ctl.jpeg)

